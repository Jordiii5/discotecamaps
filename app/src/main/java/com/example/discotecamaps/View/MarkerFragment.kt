package com.example.discotecamaps.View

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.discotecamaps.R
import com.example.discotecamaps.databinding.FragmentMarkerBinding

class MarkerFragment : Fragment() {
    lateinit var binding: FragmentMarkerBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMarkerBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val longitude = arguments?.getFloat("longitude")
        val latitude = arguments?.getFloat("latitude")

        binding.cameraAccess.setOnClickListener {
            findNavController().navigate(R.id.action_markerFragment_to_cameraFragment)
        }
    }
}